package Yang_FlinkPro;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class flinkSource {
    public static void main(String[] args) throws Exception {
        //创建数据流环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //设置并行度
        env.setParallelism(1);

        DataStreamSource<String> dataStream = env.readTextFile("input/cliks.txt");

        SingleOutputStreamOperator<Tuple2<String, Long>> dataMap = dataStream.map(new MapFunction<String, Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> map(String value) throws Exception {
                String[] split = value.split(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");
                String name = split[0];
                return Tuple2.of(name, 1L);
            }
        });

//        dataMap.print("这是map后的数据:");
        KeyedStream<Tuple2<String, Long>, String> keydata = dataMap.keyBy(new KeySelector<Tuple2<String, Long>, String>() {
            @Override
            public String getKey(Tuple2<String, Long> stringLongTuple2) throws Exception {
                return stringLongTuple2.f0;
            }
        });

        SingleOutputStreamOperator<Tuple2<String, Long>> result = keydata.reduce(new ReduceFunction<Tuple2<String, Long>>() {
            @Override
            public Tuple2<String, Long> reduce(Tuple2<String, Long> Tuple2, Tuple2<String, Long> t1) throws Exception {
                return org.apache.flink.api.java.tuple.Tuple2.of(Tuple2.f0, Tuple2.f1 + t1.f1);
            }
        });

        result.print("最后统计的结果:");

        env.execute();
    }
}
